package co.com.scalatraining.monads

import org.scalatest.FunSuite

import scala.util.{Failure, Success, Try}

class TryOptionSuite extends FunSuite{

  def foo(i:Int): Try[Option[Int]] ={
    if (i%2!=0){

      Failure(new Error("el numero es impar"))
    }
    else{
      if(i<=10){
        Try{Option(i)}
      }else{
        Try{None}
      }
    }

  }

  test("Option to foo Tyr Option pares"){


    val resultado = for {
      x <- foo(2)
      y <- foo(4)
      z <- foo(6)

    }yield (x.flatMap(i=>y.flatMap(a=>z.map(b=>i+a+b))))

    assert(resultado==Success(Some(12)))

  }
  test("Option to foo Tyr Option impares"){


    val resultado = for {
      x <- foo(2)
      y <- foo(1)
      z <- foo(6)

    }yield (x.flatMap(i=>y.flatMap(a=>z.map(b=>i+a+b))))

    assert(resultado.isFailure)

  }
  test("Option to foo Tyr Option mayor a diez"){


    val resultado = for {
      x <- foo(2)
      y <- foo(4)
      z <- foo(16)

    }yield (x.flatMap(i=>y.flatMap(a=>z.map(b=>i+a+b))))

    assert(Success(None)==resultado)

  }


}
