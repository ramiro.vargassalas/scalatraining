package co.com.scalatraining.monads

import org.scalatest.FunSuite

class EitherSuite extends FunSuite{

  test("Either left"){
    val e = Left(1)
    assert(e.isLeft)
  }

  test("Either right"){
    val e = Right(1)
    assert(e.isRight)
  }

  def foo(i:Int): Either[String, Int] = {
    if(i%2==0) Right(i)
    else Left(s"El numero $i es impar")
  }

  test("Either left or right"){

    assert(foo(2).isRight)
  }
  test("Either left or right 2"){

    assert(foo(1).isLeft)
  }

  test("Either se debe poder fold por la derecha"){
    val r: Int  = foo(2).fold[Int]( s => {
      assert(false)
      1
    }
      , i => {
        assert(true)
        6
      }
    )

    assert(r == 6)

  }
  test("Either se debe poder fold por la izquierda"){
    val r: Int  = foo(1).fold[Int]( s => {
      assert(true)
      1
    }
      , i => {
        assert(false)
        6
      }
    )

    assert(r == 1)

  }
  test("Either swap para cambiar de drecha a izq y viceversa"){
    val r1: Either[String, Int] =foo(2)
    val r2=r1.swap
    val r3: Either[Int, String] =r2
    assert(r3.isLeft)

  }
  test("Either for pares"){


    val resultado = for {
      x <- foo(2)
      y <- foo(4)
      z <- foo(6)

    }yield (x+y+z)

    print(resultado==Right(12))

  }
  test("Either for impares"){


    val resultado = for {
      x <- foo(2)
      y <- foo(1)
      z <- foo(6)

    }yield (x+y+z)

    print(resultado==Left("El numero 1 es impar"))

  }
  test("Either for impares 2"){

    val right=foo(2)
    val left=foo(1)

    val mapr=right.map(x=>x*2)

    val mapl=left.map(x=>x+"No hace la transformación")
    assert(Right(4)==mapr)
    assert(Left("El numero 1 es impar")==mapl)
  }

  test("Swap un Either"){
    val res: Either[Int, String] = foo(1).swap

    assert(res.isRight)



  }

}
