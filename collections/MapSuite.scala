package co.com.scalatraining.collections

import org.scalatest.FunSuite

class MapSuite extends FunSuite {

  test ("Creacion vacia") {
      val mapa1 = Map()
      val mapa2 = Map.empty
      assert(mapa1.isEmpty)
      assert(mapa2.isEmpty)
    assert(mapa1==mapa2)
  }

  test("Un Map se debe poder operar en un for-comp"){
    val mapa = Map(1->"uno", 2->"dos")

    val res = for{
      i <- mapa
      if i._1 == 1
    } yield(i)


    assert(res.keys.size === 1)
    assert(res.keys.head === 1)
    assert(res.get(mapa.keys.head).get === "uno")
  }

  test("mapValue en un Map") {
    val map: Map[String, Int] = {
      Map("1" -> 1, "2" -> 2, "3" -> 3)
    }
    assertResult(Map("1" -> 1, "2" -> 4, "3" -> 9)) {
      map.mapValues(valor => valor * valor)
    }
  }

  test("head en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    assertResult("1" -> 1) {
      map.head
    }
  }
  test("head en un Map2") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    val maphead=map.head

    assert(maphead._1=="1")
    assert(maphead._2==1)

  }
  test("head en un Map Vacio") {
    val map = Map.empty
    assertThrows[NoSuchElementException] {
      val maphead=map.head
    }

  }
  test("head en un Map transformacion") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    val map2 = Map("1" -> 1, "2" -> 4, "3" -> 9)
    val result=map.map(i=>(i._1,i._2*i._2))
    assertResult(map2){
      result

    }
  }



  test("tail en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    assertResult(Map("2" -> 2, "3" -> 3)) {
      map.tail
    }
  }

  test("split en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    val (map2, map3) = map.splitAt(2)
    assert(map2 == Map("1" -> 1, "2" -> 2) && map3 == Map("3" -> 3))
  }

  test("crear nuevo Map con un item mas") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    assertResult(Map("1" -> 1, "2" -> 2, "3" -> 3, "4" -> 4)) {
      map + ("4" -> 4)
    }
  }


  test("drop en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    assertResult(Map("2" -> 2, "3" -> 3)) {
      map.drop(1)
    }
  }

  test("dropRight en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    assertResult(Map("1" -> 1, "2" -> 2)) {
      map.dropRight(1)
    }
  }
  test("filter Map") {
    case class nombre(nombre:String)

    val map = Map(nombre("jonathan") -> 1, nombre("jp") -> 2, nombre("natally") -> 3)
    val map2 = Map(nombre("jonathan") -> 1, nombre("jp") -> 2)
    val resp2=map.filter(j => (j._1.nombre.startsWith("j")));
    assert(resp2==map2)


  }


  test("filter en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3, "4" -> 4)
    assertResult(Map("2" -> 2, "4" -> 4)) {
      map.filter(dato =>
        dato._2 % 2 == 0
      )
    }
  }

  test("foreach en un Map") {
    val map = Map("1" -> 1, "2" -> 2, "3" -> 3)
    assertResult(6) {
      var sum = 0
      map.foreach((x) =>
        sum += x._2
      )
      sum
    }
  }

}
