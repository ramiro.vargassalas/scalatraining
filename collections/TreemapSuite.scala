package co.com.scalatraining.collections

import org.scalatest.FunSuite

import scala.collection.immutable.TreeMap

class TreemapSuite extends FunSuite {


  //Test con TreeMap vacio
  test("Listmap Vacio"){

    val test=TreeMap.empty[Int,String]
    assert(test.isEmpty)
  }

  //Sintaxis basica para TreeMap
  test("Creando Sorted map"){

    val treemap=TreeMap( ("3"->"rugby"),("2"->"football"), ("1"->"tennis"),("4"->"atletism"))

    print(treemap)

    val contrast=Map("1"->"tennis","2"->"football","3"->"rugby")

    //assert(treemap==contrast)
  }
  //Note que se adiciona un elemento y el TreeMap queda en orden ,lo ordena estructura de arboles
  test("Adicionando Sorted map"){

    val sorted=TreeMap( (3->"rugby"),(2->"football"), (4->"tennis"))

    val sorted2=sorted+(1->"swim")

    val contrast=Map(1->"swim",2->"football",3->"rugby",4->"tennis")

    assert(sorted2==contrast)
  }

  //Filtrando por llaves
  test("Eliminando Sorted map data con filter"){

    val a=TreeMap( ("data"->"test1"), ("data2"->"test2"))

    val b = a.filterKeys(_ != "data")

    val mapres=Map("data2"->"test2")

    assert(b==mapres)
  }
  //Con drop se elimina el numero como indice del TreeMap
  test("Eliminando Sorted map data con drop"){

    val a=TreeMap( ("data"->"test1"), ("data2"->"test2"))

    val b = a.drop(1)

    val mapres=Map("data2"->"test2")

    assert(b==mapres)
  }

  //con map podemos actualizar valores y los organiza por arboles
  test("Actualizando Sorted map data"){

    val a=TreeMap((2->"test1"), (1->"test2"))

    val b = a.map(i=>(i._1,i._2+" new"))
    val mapres=Map(1 -> "test2 new", 2 -> "test1 new")
    assert(b==mapres)
  }


}
