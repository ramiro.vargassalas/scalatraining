package co.com.scalatraining.collections

import org.scalatest.FunSuite

import scala.collection.immutable.ListMap

class ListMapSuite extends FunSuite {

  //Test con Listmap vacio
  test("Listmap Vacio"){

    val test=ListMap.empty[Int,String]
    assert(test.isEmpty)
  }

  //Sintaxis basica para Listedmap no ordena
  test("Creando Sorted map"){

    val listmap=ListMap( "3"->"rugby","2"->"football", "1"->"tennis")

    print(listmap)

    val contrast=Map("1"->"tennis","3"->"rugby","2"->"football")

    assert(listmap==contrast)
  }
  //Note que se adiciona un elemento y el Listmap lo pone al ultimo
  test("Adicionando Sorted map"){

    val sorted=ListMap( (3->"rugby"),(2->"football"), (4->"tennis"))

    val sorted2=sorted+(1->"swim")
    val sorted3=sorted2.toList


    val contrast=Map(1->"swim",3->"rugby",2->"football",4->"tennis")

    assert(sorted2==contrast)
  }

  //Filtrando por llaves
  test("Eliminando Sorted map data con filter"){

    val a=ListMap( ("data"->"test1"), ("data2"->"test2"))


    val b = a.filterKeys(_ != "data")

    val mapres=Map("data2"->"test2")

    assert(b==mapres)
  }
  //Con drop se elimina el numero como indice del Listmap en el orden en que llegue
  test("Eliminando Sorted map data con drop"){

    val a=ListMap( ("data"->"test1"), ("data2"->"test2"))

    val b = a.drop(1)

    val mapres=Map("data2"->"test2")

    assert(b==mapres)
  }

  //con map podemos actualizar con map
  test("Actualizando Sorted map data"){

    val a=ListMap((2->"test1"), (1->"test2"))

    val b = a.map(i=>(i._1,i._2+" new"))
    val mapres=Map(1 -> "test2 new", 2 -> "test1 new")
    assert(b==mapres)
  }


}
