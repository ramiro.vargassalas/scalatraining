package co.com.scalatraining.collections

import org.scalatest.FunSuite

import scala.collection.immutable.{BitSet, HashSet, ListSet, TreeSet}

class SetWithHashTreeBitListSuite extends FunSuite {


  //deja en orden
  test("Add with set"){
    val set=Set(1,2,3)
    val mas=set+(4)
    print(mas)
  }

  //deja en orden mas rapido el ordenamiento
  test("Add with Hashset"){
    val hashset=HashSet(1,2,3)
    val mashash=hashset+(4)
    print(mashash)
  }
  //Imprime en orden
  test("Add with Treeset"){
    val treeset=TreeSet(1,2,3)
    val mastree=treeset+(4)
    print(mastree)
  }
  //Imprime en orden
  test("Add with Bitset"){
    val bitset=BitSet(1,2,3)
    val mastree=bitset+(4)
    print(mastree)
  }
  //Voltea el orden
  test("Add with Listset"){
    val listset=ListSet(1,2,3)
    val maslist=listset+(4)
    print(maslist)
  }
  test("Add with Listset function"){
    val s=ListSet.empty[Int]
    val r1=s+1
    val r2=r1+2
    val r3=r2+3
    println(r3)
    println(r3.head)
  }
  test("Add with Listset one"){
    val s=ListSet.empty[Int]
    val r1=s+1+2+3

    println(r1)
    println(r1.head)
  }

}
