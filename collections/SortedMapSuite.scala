package co.com.scalatraining.collections

import org.scalatest.FunSuite

import scala.collection.SortedMap


class SortedMapSuite extends FunSuite  {

  //para crear uno vacio se adiciona el tipo del SortedMap
  test("Vacio Sorted map"){
    val sorted=SortedMap.empty[Int,Any]
    assert(sorted.isEmpty)

  }
  //Sintaxis basica para Sorted Map
  test("Creando Sorted map"){

    val sorted=SortedMap( (3->"rugby"),(2->"football"), (1->"tennis"))

    val contrast=Map(1->"tennis",2->"football",3->"rugby")

    assert(sorted==contrast)
  }

  //Note que se adiciona un elemento y el Sortedmap queda en orden
  test("Adicionando Sorted map"){

    val sorted=SortedMap( (3->"rugby"),(2->"football"), (4->"tennis"))

    val sorted2=sorted+(1->"swim")

    val contrast=Map(1->"swim",2->"football",3->"rugby",4->"tennis")

    assert(sorted2==contrast)
  }

  //Filtrando por llaves
  test("Eliminando Sorted map data con filter"){

    val a=SortedMap( ("data"->"test1"), ("data2"->"test2"))

    val b = a.filterKeys(_ != "data")

    val mapres=Map("data2"->"test2")

    assert(b==mapres)
  }
  //Con drop se elimina el numero como indice del sorted map
  test("Eliminando Sorted map data con drop"){

    val a=SortedMap( ("data"->"test1"), ("data2"->"test2"))

    val b = a.drop(1)

    val mapres=Map("data2"->"test2")

    assert(b==mapres)
  }

  //con map podemos actualizar valores y conservan su orden
  test("Actualizando Sorted map data"){

    val a=SortedMap((2->"test1"), (1->"test2"))

    val b = a.map(i=>(i._1,i._2+" new"))
    val mapres=Map(1 -> "test2 new", 2 -> "test1 new")
    assert(b==mapres)
  }


}
